package br.com.empresa.api.vo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RoomTotal {

	private Long roomID;

	private String categoryName;

	private BigDecimal totalPrice;

	private PriceDetail priceDetail;

	public RoomTotal(Room room, Integer adult, Integer child, BigDecimal days) {
		
		this.roomID = room.getRoomID();
		this.categoryName = room.getCategoryName();
		this.priceDetail = new PriceDetail(room.getPrice());
		
		// Price adult + comissão
		BigDecimal totalPriceAdult = (room.getPrice().getAdult().multiply(new BigDecimal(adult)).divide(new BigDecimal("0.7"), 2, RoundingMode.HALF_EVEN));

		// Price child + comissão
		BigDecimal totalPriceChild = (room.getPrice().getChild().multiply(new BigDecimal(child)).divide(new BigDecimal("0.7"), 2, RoundingMode.HALF_EVEN));

		// Price adult + child
		BigDecimal totalPriceAdultPlusChild = totalPriceAdult.add(totalPriceChild);

		// Price (adult + child) * days
		this.totalPrice = totalPriceAdultPlusChild.multiply(days);
		
	}

	public Long getRoomID() {
		return roomID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public PriceDetail getPriceDetail() {
		return priceDetail;
	}

}