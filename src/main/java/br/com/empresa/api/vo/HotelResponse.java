package br.com.empresa.api.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class HotelResponse {

	private Long id;

	private String cityName;

	private List<RoomTotal> rooms;

	public HotelResponse(Hotel hotel, Integer adult, Integer child, BigDecimal days) {
		this.id = hotel.getId();
		this.cityName = hotel.getCityName();
		this.rooms = new ArrayList<>();
		hotel.getRooms().forEach(action -> rooms.add(new RoomTotal(action, adult, child, days)));
	}

	public Long getId() {
		return id;
	}

	public String getCityName() {
		return cityName;
	}

	public List<RoomTotal> getRooms() {
		return rooms;
	}

}