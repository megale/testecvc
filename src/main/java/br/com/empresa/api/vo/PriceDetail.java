package br.com.empresa.api.vo;

import java.math.BigDecimal;

public class PriceDetail {

	private BigDecimal pricePerDayAdult;

	private BigDecimal pricePerDayChild;

	public PriceDetail(Price price) {
		this.pricePerDayAdult = price.getAdult();
		this.pricePerDayChild = price.getChild();
	}

	public BigDecimal getPricePerDayAdult() {
		return pricePerDayAdult;
	}

	public BigDecimal getPricePerDayChild() {
		return pricePerDayChild;
	}

}