package br.com.empresa.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteBackendDeveloperApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteBackendDeveloperApplication.class, args);
	}
}
