package br.com.empresa.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ParameterRequiredException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ParameterRequiredException(String message) {
		super(message);
	}

}