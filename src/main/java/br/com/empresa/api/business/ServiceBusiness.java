package br.com.empresa.api.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.empresa.api.exception.ParameterRequiredException;
import br.com.empresa.api.exception.ResourceNotFoundException;
import br.com.empresa.api.vo.Hotel;
import br.com.empresa.api.vo.HotelResponse;

@Service
public class ServiceBusiness {

	@Autowired
	HotelBusiness hotelBusiness;
	
	public List<HotelResponse> getDetail(Long cityCode, BigDecimal days, Integer adult, Integer child) {

		if (days == null || days.intValue() <= 0)
			throw new ParameterRequiredException("A data de checkout deve ser maior do que a data de checkin");
		
		if (adult < 1 && child < 1)
			throw new ParameterRequiredException("Nenhum adulto ou criança foi selecionado");
		
		List<HotelResponse> hoteisResponse = new ArrayList<>();

		// Consultar lista de hotéis no broker
		List<Hotel> hoteis = hotelBusiness.getHotelsByIdCidade(cityCode);
		
		if (hoteis == null || hoteis.isEmpty())
			throw new ResourceNotFoundException("Nenhum hotel encontrado com o cityCode = " + cityCode);

		// Realizar calculo de preço e preenche objeto de resposta
		hoteis.forEach(hotel -> hoteisResponse.add(new HotelResponse(hotel, adult, child, days)));

		return hoteisResponse;

	}

}