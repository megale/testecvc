package br.com.empresa.api.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.empresa.api.exception.ParameterRequiredException;
import br.com.empresa.api.exception.ResourceNotFoundException;
import br.com.empresa.api.vo.Hotel;
import br.com.empresa.api.vo.HotelResponse;

@Service
public class HotelBusiness {

	public List<HotelResponse> getDetail(Long hotelCode, BigDecimal days, Integer adult, Integer child) {

		if (days == null || days.intValue() <= 0) 
			throw new ParameterRequiredException("A data de checkout deve ser maior do que a data de checkin");

		if (adult < 1 && child < 1)
			throw new ParameterRequiredException("Nenhum adulto ou criança foi selecionado");
		
		List<HotelResponse> hoteisResponse = new ArrayList<>();

		// Consultar lista de hotéis no broker
		List<Hotel> hoteis = getById(hotelCode);
		if (hoteis == null || hoteis.isEmpty())
			throw new ResourceNotFoundException("Nenhum hotel encontrado com o hotelId = " + hotelCode);

		// Realizar calculo de preço e preenche objeto de resposta
		hoteis.forEach(hotel -> hoteisResponse.add(new HotelResponse(hotel, adult, child, days)));

		return hoteisResponse;

	}

	public List<Hotel> getHotelsByIdCidade(Long idCidade) {

		RestTemplate restTemplate = new RestTemplate();
		Hotel[] hotelArray = restTemplate.getForObject("https://cvcbackendhotel.herokuapp.com/hotels/avail/" + idCidade, Hotel[].class);
		ArrayList<Hotel> hotels = new ArrayList<Hotel>(Arrays.asList(hotelArray));

		return hotels;

	}

	private List<Hotel> getById(Long id) {

		RestTemplate restTemplate = new RestTemplate();
		Hotel[] hotelArray = restTemplate.getForObject("https://cvcbackendhotel.herokuapp.com/hotels/" + id, Hotel[].class);
		ArrayList<Hotel> hotels = new ArrayList<Hotel>(Arrays.asList(hotelArray));

		return hotels;

	}
	
}