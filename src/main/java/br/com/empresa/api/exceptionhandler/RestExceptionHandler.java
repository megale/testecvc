package br.com.empresa.api.exceptionhandler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.empresa.api.exception.ParameterRequiredException;
import br.com.empresa.api.exception.ResourceNotFoundException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException rnfe, HttpServletRequest request) {
		HttpStatus httpStatus = HttpStatus.NOT_FOUND;
		ErrorDetail errorDetail = new ErrorDetail(httpStatus, rnfe.getMessage(), rnfe.getClass().getName());
		return new ResponseEntity<>(errorDetail, null, httpStatus);
	}
	
	@ExceptionHandler(ParameterRequiredException.class)
	public ResponseEntity<?> handleResourceNotFoundException(ParameterRequiredException rnfe, HttpServletRequest request) {
		HttpStatus httpStatus = HttpStatus.CONFLICT;
		ErrorDetail errorDetail = new ErrorDetail(httpStatus, rnfe.getMessage(), rnfe.getClass().getName());
		return new ResponseEntity<>(errorDetail, null, httpStatus);
	}
	
}