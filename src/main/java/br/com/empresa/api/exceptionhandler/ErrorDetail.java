package br.com.empresa.api.exceptionhandler;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpStatus;

public class ErrorDetail {

	private String timestamp;

	private int status;

	private String title;

	private String detail;

	private String developerMessage;

	public ErrorDetail(HttpStatus httpStatus, String detail, String developerMessage) {
		
		this.timestamp = getHoraAtual();
		this.status = httpStatus.value();
		this.title = getTitulo(httpStatus);
		this.detail = detail;
		this.developerMessage = getMensagemDoDesenvolvedor(developerMessage);
		
	}

	private String getMensagemDoDesenvolvedor(String developerMessage) {

		if (developerMessage != null)
			return developerMessage.replace("br.com.lelloimoveis.api.exceptions.", "");
		
		return null;
		
	}

	private String getTitulo(HttpStatus httpStatus) {
		return httpStatus.name().replaceAll("_", " ").toLowerCase();
	}

	private String getHoraAtual() {
		return new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(new Date());
	}

	// Getters and setters
	public String getTimestamp() {
		return timestamp;
	}

	public int getStatus() {
		return status;
	}

	public String getTitle() {
		return title;
	}

	public String getDetail() {
		return detail;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}
}