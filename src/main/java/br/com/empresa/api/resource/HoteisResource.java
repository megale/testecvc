package br.com.empresa.api.resource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.api.business.HotelBusiness;
import br.com.empresa.api.vo.HotelResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * Classe responsável por expor os serviços relacionados a hotéis
 * 
 * @author Gabriel Coutinho Megale
 *
 */
@Api(value = "hoteis")
@RequestMapping(value = "hoteis")
@RestController
public class HoteisResource {

	@Autowired
	private HotelBusiness hotelBusiness;

	@GetMapping(value = "detail", produces = "application/json")
	@ApiOperation(value = "Calcular o preço total do hotel", response = HotelResponse.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "hotelCode", value = "Código do hotel", required = true), @ApiImplicitParam(name = "checkin", value = "Data do checkin (yyyy-MM-dd)", required = true), @ApiImplicitParam(name = "checkout", value = "Data do checkout (yyyy-MM-dd)", required = true), @ApiImplicitParam(name = "adult", value = "Quantidade de adultos", required = true), @ApiImplicitParam(name = "child", value = "Quantidade de crianças", required = true) }) 
	public @ResponseBody ResponseEntity<HotelResponse> getTotalPrice(@RequestParam(value = "hotelCode", required = true) Long hotelCode, @RequestParam(value = "checkin", required = true) String checkin, @RequestParam(value = "checkout", required = true) String checkout, @RequestParam(value = "adult", required = true) Integer adult, @RequestParam(value = "child", required = true) Integer child) {
		
		BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(LocalDate.parse(checkin), LocalDate.parse(checkout)));
		List<HotelResponse> detail = hotelBusiness.getDetail(hotelCode, days, adult, child);
		if (detail != null && !detail.isEmpty())
			return ResponseEntity.ok(detail.get(0));
		
		return ResponseEntity.notFound().build();
		
	}
	
	/*
	@GetMapping(value = "{id:[0-9]+}", produces = "application/json")
	@ApiOperation(value = "Pesquisar hotéis pelo id", response = Hotel.class)
	@ApiImplicitParam(name = "id", value = "Id do hotel", required = true)
	public @ResponseBody List<Hotel> getById(@PathVariable("id") Long id) {
		return hotelBusiness.getById(id);
	}

	@GetMapping(value = "cidade/{idCidade:[0-9]+}", produces = "application/json")
	@ApiOperation(value = "Pesquisar hotéis pelo id da cidade", response = Hotel.class)
	@ApiImplicitParam(name = "idCidade", value = "Id da cidade", required = true)
	public @ResponseBody List<Hotel> getHotelsByIdCidade(@PathVariable("idCidade") Long idCidade) {
		return hotelBusiness.getHotelsByIdCidade(idCidade);
	}
	*/
}