package br.com.empresa.api.resource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresa.api.business.ServiceBusiness;
import br.com.empresa.api.vo.HotelResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * Classe responsável por expor os serviços
 * 
 * @author Gabriel Coutinho Megale
 *
 */
@Api(value = "services", description = "Detalhes do serviço")
@RequestMapping(value = "services")
@RestController
public class ServiceResource {

	@Autowired
	private ServiceBusiness serviceBusiness;

	@GetMapping(value = "detail", produces = "application/json")
	@ApiOperation(value = "Calcular o preço total do serviço", response = HotelResponse.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "cityCode", value = "Código da cidade", required = true), @ApiImplicitParam(name = "checkin", value = "Data do checkin (yyyy-MM-dd)", required = true), @ApiImplicitParam(name = "checkout", value = "Data do checkout (yyyy-MM-dd)", required = true), @ApiImplicitParam(name = "adult", value = "Quantidade de adultos", required = true), @ApiImplicitParam(name = "child", value = "Quantidade de crianças", required = true) }) 
	public @ResponseBody ResponseEntity<List<HotelResponse>> getTotalPrice(@RequestParam(value = "cityCode", required = true) Long cityCode, @RequestParam(value = "checkin", required = true) String checkin, @RequestParam(value = "checkout", required = true) String checkout, @RequestParam(value = "adult", required = true) Integer adult, @RequestParam(value = "child", required = true) Integer child) {
		
		BigDecimal days = new BigDecimal(ChronoUnit.DAYS.between(LocalDate.parse(checkin), LocalDate.parse(checkout)));
		return ResponseEntity.ok().body(serviceBusiness.getDetail(cityCode, days, adult, child));
		
	}

}